from flask import Flask, render_template, request, redirect, flash
from mysqlconnection import MySQLConnection
import re

app = Flask(__name__)
app.secret_key = "supersecretkey"
mysql = MySQLConnection(app, "mydb")

EMAIL_REGEX = re.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/success')
def success_page():
    return render_template("success.html", users=mysql.query_db("SELECT * FROM users"))

@app.route('/delete/<id>')
def delete_user(id):
    data = {
        "id": id
    }
    deleted_user = mysql.query_db("SELECT email FROM users WHERE id = :id", data)[0]
    mysql.query_db("DELETE FROM users WHERE id = :id", data)
    flash(u"You have successfully deleted {}".format(deleted_user['email']), "success")
    return redirect('/success')

@app.route('/process', methods=['POST'])
def process_form():
    if not re.match(EMAIL_REGEX, request.form['email']):
        flash(u"Please enter a valid email.", "error")
        return redirect('/')
    query = "INSERT INTO users (email) \
            VALUES (:email)"
    data = {
        "email": request.form['email']
    }
    mysql.query_db(query, data)
    flash(u"You have successfully created a new email: {}".format(request.form['email']), "success")
    return redirect('/success')

app.run(debug=True)
